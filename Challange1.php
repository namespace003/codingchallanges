<?php
/**
 * Created by PhpStorm.
 * User: dev
 * Date: 27/08/18
 * Time: 9:12 AM
 */

namespace App;

/*
 * Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
 * For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
*/

class Challange1
{

	/**
	 * @var
	 * */
	private $k;

	/**
	 * @param float $k
	 *
	 */
	function __construct(float $k)
	{
		$this->k = $k;

	}

	/**
	 * @param array $arrayList
	 *
	 * @return array
	 * */
	public function checkNumberSummation(array $arrayList)
	{

		$resultArray = [];
		$count       = 0;
		foreach ($arrayList as $a) {
			$count++;
			for ($i = $count; $i < count($arrayList); $i++) {
				if ($a + $arrayList[$i] == $this->k) {
					$resultArray [] = [$a, $arrayList[$i]];
				}
			}
		}

		return $resultArray;
	}
}

$list = range(0, 50000);

define("start", date('Y-m-d H:i:s.u'));
$challange1 = new Challange1(255);

$result = $challange1->checkNumberSummation($list);
define("end", date('Y-m-d H:i:s.u'));

print_r($result);
echo count($result);

echo "\nExecution Time= " . (strtotime(end) - strtotime(start));

